# Techtonic Group

## Setting up your development environment
This was written for Windows 7 OS

### Development Directory
`c:/www/<projects>`

### Host file
`c:\Windows\System32\drivers\etc\hosts`

### Microsoft APIs
[Learn About ASP.NET MVC](http://www.asp.net/mvc)


### External Tools
[PHP Tools for Visual Studio](https://visualstudiogallery.msdn.microsoft.com/6eb51f05-ef01-4513-ac83-4c5f50c95fb5)

[Resharper-JetBrains](https://www.jetbrains.com/resharper/)
[Beyond Compare](http://www.scootersoftware.com/index.php)


### Source Control
[Git for Windows](https://git-scm.com/download/win)
[Git for Mac](https://git-scm.com/download/mac)
[Git Documentation](https://git-scm.com/doc)


### Open Source Projects
[AngularJS](https://angularjs.org/)
[Bootstrap](http://getbootstrap.com/)


### Third Party Sources
[Kentico Documentation](https://devnet.kentico.com/documentation)
